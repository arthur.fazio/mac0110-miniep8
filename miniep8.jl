using Test

function testafuncoes()
    @test compareByValue("2♠", "A♠")
    @test !compareByValue("K♥", "10♥")
    @test !compareByValue("10♠", "10♥")
    @test compareByValueAndSuit("2♠", "A♠")
    @test !compareByValueAndSuit("K♥", "10♥")
    @test compareByValueAndSuit("10♠", "10♥")
    @test compareByValueAndSuit("A♠", "2♥")
    @test !compareByValueAndSuit("2♥", "A♠")
    @test !compareByValueAndSuit("5♣", "6♥")
    @test !compareByValueAndSuit("A♧", "10♣")
    @test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♦", "J♠", "K♠", "A♠", "A♠", "10♥"]
    println("Final dos testes")
end

function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function parse_slice_to_int(z)
    if z == "J"
        return 11
    elseif z == "Q"
        return 12
    elseif z == "K"
        return 13
    elseif z == "A"
        return 14
    else
        return parse(Int64, z)
    end
end

function parse_suit_to_int(s)
    if s == '♢' || s == '♦'
        return 1
    elseif s == '♠' || s == '♤'
        return 2
    elseif s == '♡' || s == '♥'
        return 3
    else
        return 4
    end
end

function compareByValue(x, y)
    x1 = parse_slice_to_int(x[1:end - 1])
    y1 = parse_slice_to_int(y[1:end - 1])
    return x1 < y1
end

function compareByValueAndSuit(x, y)
    xnum = [parse_slice_to_int(x[1:end - 1]), parse_suit_to_int(x[end])]
    ynum = [parse_slice_to_int(y[1:end - 1]), parse_suit_to_int(y[end])]
    return xnum[2] < ynum[2] || (xnum[2] == ynum[2] && xnum[1] < ynum[1])
end

function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValueAndSuit(v[j], v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end
